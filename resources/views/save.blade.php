@extends('layouts.layout')
@section('content')
    <div class="modal fade" id="mostrarmodal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="text-center">¡Felicitaciones!</h3>
                </div>
                <div class="modal-body">
                    <h4 class="text-center text-danger"><strong>Alumno añadido correctamente</strong></h4>
                </div>
                <div class="modal-footer trans">
                    <a href="#" data-dismiss="modal" class="btn btn-danger btn-block "><strong>Cerrar</strong></a>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <h1 class="trans text-danger">Listado de alumnos registrados</h1>
    <br><br>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="table-responsive">
                <table class="table table-dark">
                    <thead>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Teléfono</th>
                    <th>Edad</th>
                    </thead>
                    @foreach($alumnos as $a)
                        <tr>
                            <td>{{$a->nombre}}</td>
                            <td>{{$a->apellidos}}</td>
                            <td>{{$a->telefono}}</td>
                            <td>{{$a->edad}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <a href="create" class="trans">
                <button class="btn btn-danger trans ">Inscribir otro alumno</button>
            </a>
        </div>
    </div>
@endsection