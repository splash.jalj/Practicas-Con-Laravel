@extends('layouts.layout')
@section('content')
    @if(count($errors)>0)
        <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $error)
                <ul>
                    <li>{{$error}}</li>
                </ul>
            @endforeach
        </div>
    @endif

    <!--cuantos elementos tiene un arreglo-->
    {!!Form::open(array('url'=>'/','method'=>'POST','autocomplete'=>'off'))!!}
    {{Form::token()}}
    <form>
        <br>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="Nombre">Nombre del alumno</label>
                    <input type="text" name="nombre" class="form-control" id="nombre">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="Apellidos">Apellidos</label>
                    <input type="text" name="apellidos" class="form-control" id="apellidos">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="Telefono">Teléfono</label>
                    <input type="tel" name="telefono" class="form-control" id="telefono"">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="edad">Edad</label>
                    <input type="number" name="edad" class="form-control" id="edad">
                </div>
            </div>
        </div>
        <button type="submit" class="col-md-offset-5 btn btn-danger btn-lg" data-toggle="modal"
                data-target="#exampleModal">REGISTRAR
        </button>
    </form>
    {!!Form::close()!!}
@endsection