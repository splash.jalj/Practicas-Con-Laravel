<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('fonts/montserrat.css')}}">
    <link rel="stylesheet" href="{{asset('fonts/lato.css')}}">
    <link rel="icon" href="{{asset('favicon.ico')}}">
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/script.js')}}"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="create">Instituto tecnológico del istmo</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#about">NOTICIAS</a></li>
                <li><a href="#services">CONTACTO</a></li>
                <li><a href="#portfolio">DIRECTORIO</a></li>
            </ul>
        </div>
    </div>
</nav>

<!---->

<main role="main">
    <div class="container-fluid">
        <div class="col-md-12">
            @yield('content')
        </div>
    </div>
</main>

<!---->

</body>
</html>