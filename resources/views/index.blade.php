@extends('layouts.layout')
@section('content')
    <br><br>
    <h1 class="text-center">INSCRIPCIÓN DE ALUMNOS</h1><br>
    <br>
    <div class="col-md-offset-5"><br>
        <a href="create">
            <button class="btn btn-danger btn-lg ">Inscribir alumno</button>
        </a>
    </div>
    <br>
    <br>
    <h1 class="text-center">Alumnos registrados</h1>
    <br><br>
    <div class="table-responsive">
        <table class="table table-dark">
            <thead>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Teléfono</th>
            <th>Edad</th>
            </thead>
            @foreach($alumnos as $a)
                <tr>
                    <td>{{$a->nombre}}</td>
                    <td>{{$a->apellidos}}</td>
                    <td>{{$a->telefono}}</td>
                    <td>{{$a->edad}}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <!--<a href="save">
        <button class="btn btn-info">Prueba</button>
    </a>-->
    <a href="mostrarNombre">
        <button class="btn btn-info">Mostrar nombre</button>
    </a>
@endsection

