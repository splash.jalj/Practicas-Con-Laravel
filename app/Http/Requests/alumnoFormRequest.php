<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class alumnoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //arreglo asociativo
            'nombre'=>'required|max:30|min:3',
            'apellidos'=>'required|max:40|min:3',
            'telefono'=>'required|numeric',
            'edad'=>'required|numeric'
        ];
    }
}
