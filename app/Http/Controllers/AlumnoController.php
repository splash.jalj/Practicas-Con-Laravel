<?php

namespace App\Http\Controllers;


use App\Http\Requests\alumnoFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Alumnos;

class AlumnoController extends Controller
{
    //
    public function index(Request $request)
    {
        //$guardar =$request ->get('guardar');
       // if ($guardar == null)

        //$nombres = array('Hola', 'Mundo', 'sadasd');
        //return view('index', ['nombres'=>$nombres]);
        $alumnos=Alumnos::all();
        //return view('index', ['alumnos'=>$alumnos]);
        return view('index', ['alumnos'=>$alumnos]);
    }
    public function begin()
    {
        return view('index');
    }
    public function create(){
        return view('formAlumno');
    }
    public function mostrar(){
        //
    }


    public function store(alumnoFormRequest $request){
        $alumno = new Alumnos;

        $nombre=$request->get('nombre');
        $alumno->nombre=$nombre;

        $apellidos=$request->get('apellidos');
        $alumno->apellidos=$apellidos;

        $telefono=$request->get('telefono');
        $alumno->telefono=$telefono;

        $edad=$request->get('edad');
        $alumno->edad=$edad;

        $alumno->save();
        $alumnos=Alumnos::all();
        return view('save', ['alumnos'=>$alumnos]);
        //return view('save');
        //return view('index', ['alumnos'=>$alumnos]);
        //return Redirect::to('/?guardar=1');
    }
}