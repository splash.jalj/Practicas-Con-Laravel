<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumnos extends Model
{
    //
    protected $table='alumno';
    public $timestamps=false;
}
